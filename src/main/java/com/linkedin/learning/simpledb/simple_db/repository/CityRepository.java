package com.linkedin.learning.simpledb.simple_db.repository;

import com.linkedin.learning.simpledb.simple_db.models.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepository extends JpaRepository<City, Integer> {
}
