package com.linkedin.learning.simpledb.simple_db.api;

import com.linkedin.learning.simpledb.simple_db.models.City;
import com.linkedin.learning.simpledb.simple_db.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.print.attribute.standard.Media;

@Controller
@RequestMapping(path = "/api")
public class RestController {

    @Autowired
    private CityRepository repository;

    @GetMapping(path="/",
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCities() {
        return ResponseEntity.ok(repository.findAll());
    }

    @PutMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> putCity(@RequestBody City city) {
        return ResponseEntity.ok(repository.saveAndFlush(city));
    }
}
