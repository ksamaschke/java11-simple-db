# Building
FROM maven:3.8-openjdk-11-slim as builder
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -DskipTests -f /usr/src/app/pom.xml clean package

# Running
FROM eclipse-temurin:11

# Copy
RUN mkdir /opt/app
COPY --from=builder /usr/src/app/target/*.jar /opt/app/app.jar

# Create a user
RUN groupadd -r user && useradd -r -g user user
USER user

# RUn
CMD ["java", "-jar", "/opt/app/app.jar"]